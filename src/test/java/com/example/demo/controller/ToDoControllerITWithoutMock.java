package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class ToDoControllerITWithoutMock {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenGetNotExistingToDo_thenNotFoundException() throws Exception {
        this.mockMvc
                .perform(get("/todos/3"))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> Objects.requireNonNull(mvcResult.getResolvedException())
                                                                        .getClass()
                                                                        .equals(ToDoNotFoundException.class));
    }

    @Test
    public void whenUpsertNoId_thenReturnNew() throws Exception {
        ToDoSaveRequest saveRequest = new ToDoSaveRequest();
        saveRequest.text = "Third todo";
        String jsonRequest = objectToJson(saveRequest);
        this.mockMvc
                .perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(3))
                .andExpect(jsonPath("text").value("Third todo"));
    }

    @Test
    public void whenUpsertWithId_thenReturnUpdated() throws Exception {
        ToDoSaveRequest saveRequest = new ToDoSaveRequest();
        saveRequest.id = 2L;
        saveRequest.text = "New Text";
        String jsonRequest = objectToJson(saveRequest);
        this.mockMvc
                .perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(2))
                .andExpect(jsonPath("text").value("New Text"));
    }

    @Test
    public void whenDeleteOne_thenRepositoryDeleteCalled() throws Exception {
        this.mockMvc
                .perform(delete("/todos/1"))
                .andExpect(status().isOk());
    }

    public String objectToJson(ToDoSaveRequest saveRequest) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(saveRequest);
    }
}

