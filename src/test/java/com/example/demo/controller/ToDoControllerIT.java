package com.example.demo.controller;

import com.example.demo.exception.ToDoNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Objects;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
public class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenGetOne_thenReturnOneById() throws Exception {
        String testText = "Test text";
        Long id = 1L;
        when(toDoRepository.findById(anyLong())).thenReturn(
                java.util.Optional.of(new ToDoEntity(id, testText))
        );

        this.mockMvc
            .perform(get("/todos/1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("id").value(1L))
            .andExpect(jsonPath("text").value(testText))
            .andExpect(jsonPath("completedAt").doesNotExist());
    }

    @Test
    void whenComplete_thenReturnWithCompletedAt() throws Exception {
        ToDoEntity testToDoEntity = new ToDoEntity(0L, "Test text");
        when(toDoRepository.findById(anyLong())).thenReturn(java.util.Optional.of(testToDoEntity));
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(x -> {
            ToDoEntity toDoEntity = x.getArgument(0, ToDoEntity.class);
            Long id = toDoEntity.getId();
            if (id.equals(testToDoEntity.getId())) {
                return testToDoEntity;
            }
            else {
                return new ToDoEntity();
            }
        });

        this.mockMvc
            .perform(put("/todos/0/complete"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("id").value(0L))
            .andExpect(jsonPath("text").value("Test text"))
            .andExpect(jsonPath("completedAt").exists());
    }

    @Test
    void whenNotFound_thenThrowException() throws Exception {
        mockMvc.perform(get("/todos/1"))
               .andExpect(status().isOk())
               .andExpect(result -> assertTrue(result.getResolvedException() instanceof ToDoNotFoundException))
               .andExpect(result -> assertEquals("Can not find todo with id 1",
                       Objects.requireNonNull(result.getResolvedException()).getMessage()));
    }
}
