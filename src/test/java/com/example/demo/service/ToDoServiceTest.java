package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import org.junit.jupiter.api.Test;


import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.mockito.InOrder;

public class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenToDosNotFound_thenThrowNotFoundException() {
        ToDoSaveRequest saveRequest = new ToDoSaveRequest();
        saveRequest.text = "Test text";
        saveRequest.id = 1L;
        assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(saveRequest));
    }

    @Test
    void whenUpsertWithId_firstCallFindByIdThenSave() throws ToDoNotFoundException {
        ToDoEntity toDoTestEntity = new ToDoEntity(1L, "Test Text");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(toDoTestEntity));
        when(toDoRepository.save(any())).thenReturn(toDoTestEntity);
        ToDoSaveRequest saveRequest = new ToDoSaveRequest();
        saveRequest.id = 1L;
        saveRequest.text = "Test Text";
        toDoService.upsert(saveRequest);
        InOrder inOrder = inOrder(toDoRepository);
        inOrder.verify(toDoRepository, times(1)).findById(anyLong());
        inOrder.verify(toDoRepository, times(1)).save(any());
    }

    @Test
    void whenGetOne_thenReturnOneById() throws ToDoNotFoundException {
        ToDoEntity toDoTestEntity = new ToDoEntity(1L, "TestText");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(toDoTestEntity));
        var toDoServiceEntity = toDoService.getOne(1L);
        assertEquals(toDoTestEntity.getId(), toDoServiceEntity.id);
        assertEquals(toDoTestEntity.getText(), toDoServiceEntity.text);
    }

    @Test
    void whenGetWithComplete_thenReturnToDos() {
        List<ToDoEntity> allTestToDos = new ArrayList<>();
        allTestToDos.add(new ToDoEntity(0L, "Test ToDo First"));
        ToDoEntity testToDoEntity = new ToDoEntity(1L, "Test ToDo Second");
        testToDoEntity.completeNow();
        allTestToDos.add(testToDoEntity);
        when(toDoRepository.findAll()).thenReturn(allTestToDos);
        var toDosWithComplete = toDoService.getToDosWithComplete();
        assertEquals(1, toDosWithComplete.size());
        assertEquals(1L, toDosWithComplete.get(0).id);
        assertNotNull(toDosWithComplete.get(0).completedAt);
    }
}
